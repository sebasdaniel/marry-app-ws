/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marryapp.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author m4605
 */
@Entity
@Table(name = "participante")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Participante.findAll", query = "SELECT p FROM Participante p"),
    @NamedQuery(name = "Participante.findById", query = "SELECT p FROM Participante p WHERE p.id = :id")})
public class Participante implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "novio")
    private Collection<Boda> bodaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "novia")
    private Collection<Boda> bodaCollection1;
    @OneToMany(mappedBy = "organizador")
    private Collection<Boda> bodaCollection2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "participanteId")
    private Collection<FotoBoda> fotoBodaCollection;
    @JoinColumn(name = "usuario_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Usuario usuarioId;
    @JoinColumn(name = "boda_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Boda bodaId;
    @JoinColumn(name = "roll_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Roll rollId;
    @JoinColumn(name = "mesa_id", referencedColumnName = "id")
    @ManyToOne
    private Mesa mesaId;

    public Participante() {
    }

    public Participante(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlTransient
    public Collection<Boda> getBodaCollection() {
        return bodaCollection;
    }

    public void setBodaCollection(Collection<Boda> bodaCollection) {
        this.bodaCollection = bodaCollection;
    }

    @XmlTransient
    public Collection<Boda> getBodaCollection1() {
        return bodaCollection1;
    }

    public void setBodaCollection1(Collection<Boda> bodaCollection1) {
        this.bodaCollection1 = bodaCollection1;
    }

    @XmlTransient
    public Collection<Boda> getBodaCollection2() {
        return bodaCollection2;
    }

    public void setBodaCollection2(Collection<Boda> bodaCollection2) {
        this.bodaCollection2 = bodaCollection2;
    }

    @XmlTransient
    public Collection<FotoBoda> getFotoBodaCollection() {
        return fotoBodaCollection;
    }

    public void setFotoBodaCollection(Collection<FotoBoda> fotoBodaCollection) {
        this.fotoBodaCollection = fotoBodaCollection;
    }

    public Usuario getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Usuario usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Boda getBodaId() {
        return bodaId;
    }

    public void setBodaId(Boda bodaId) {
        this.bodaId = bodaId;
    }

    public Roll getRollId() {
        return rollId;
    }

    public void setRollId(Roll rollId) {
        this.rollId = rollId;
    }

    public Mesa getMesaId() {
        return mesaId;
    }

    public void setMesaId(Mesa mesaId) {
        this.mesaId = mesaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Participante)) {
            return false;
        }
        Participante other = (Participante) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.marryapp.entity.Participante[ id=" + id + " ]";
    }
    
}
