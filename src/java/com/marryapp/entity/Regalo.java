/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marryapp.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author m4605
 */
@Entity
@Table(name = "regalo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Regalo.findAll", query = "SELECT r FROM Regalo r"),
    @NamedQuery(name = "Regalo.findById", query = "SELECT r FROM Regalo r WHERE r.id = :id"),
    @NamedQuery(name = "Regalo.findByTitulo", query = "SELECT r FROM Regalo r WHERE r.titulo = :titulo"),
    @NamedQuery(name = "Regalo.findByDescripcion", query = "SELECT r FROM Regalo r WHERE r.descripcion = :descripcion"),
    @NamedQuery(name = "Regalo.findByProveedor", query = "SELECT r FROM Regalo r WHERE r.proveedor = :proveedor"),
    @NamedQuery(name = "Regalo.findByValorEstimado", query = "SELECT r FROM Regalo r WHERE r.valorEstimado = :valorEstimado")})
public class Regalo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "titulo")
    private String titulo;
    @Size(max = 45)
    @Column(name = "descripcion")
    private String descripcion;
    @Size(max = 45)
    @Column(name = "proveedor")
    private String proveedor;
    @Size(max = 45)
    @Column(name = "valor_estimado")
    private String valorEstimado;
    @JoinColumn(name = "categoria_regalo_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CategoriaRegalo categoriaRegaloId;
    @JoinColumn(name = "boda_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Boda bodaId;

    public Regalo() {
    }

    public Regalo(Integer id) {
        this.id = id;
    }

    public Regalo(Integer id, String titulo) {
        this.id = id;
        this.titulo = titulo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getValorEstimado() {
        return valorEstimado;
    }

    public void setValorEstimado(String valorEstimado) {
        this.valorEstimado = valorEstimado;
    }

    public CategoriaRegalo getCategoriaRegaloId() {
        return categoriaRegaloId;
    }

    public void setCategoriaRegaloId(CategoriaRegalo categoriaRegaloId) {
        this.categoriaRegaloId = categoriaRegaloId;
    }

    public Boda getBodaId() {
        return bodaId;
    }

    public void setBodaId(Boda bodaId) {
        this.bodaId = bodaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Regalo)) {
            return false;
        }
        Regalo other = (Regalo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.marryapp.entity.Regalo[ id=" + id + " ]";
    }
    
}
