/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marryapp.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author m4605
 */
@Entity
@Table(name = "presupuesto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Presupuesto.findAll", query = "SELECT p FROM Presupuesto p"),
    @NamedQuery(name = "Presupuesto.findById", query = "SELECT p FROM Presupuesto p WHERE p.id = :id"),
    @NamedQuery(name = "Presupuesto.findByTitulo", query = "SELECT p FROM Presupuesto p WHERE p.titulo = :titulo"),
    @NamedQuery(name = "Presupuesto.findByCategoria", query = "SELECT p FROM Presupuesto p WHERE p.categoria = :categoria"),
    @NamedQuery(name = "Presupuesto.findByValorEstimado", query = "SELECT p FROM Presupuesto p WHERE p.valorEstimado = :valorEstimado"),
    @NamedQuery(name = "Presupuesto.findByValorReal", query = "SELECT p FROM Presupuesto p WHERE p.valorReal = :valorReal"),
    @NamedQuery(name = "Presupuesto.findByDescripcion", query = "SELECT p FROM Presupuesto p WHERE p.descripcion = :descripcion")})
public class Presupuesto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "titulo")
    private String titulo;
    @Size(max = 45)
    @Column(name = "categoria")
    private String categoria;
    @Basic(optional = false)
    @NotNull
    @Column(name = "valor_estimado")
    private int valorEstimado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "valor_real")
    private int valorReal;
    @Size(max = 45)
    @Column(name = "descripcion")
    private String descripcion;
    @JoinColumn(name = "boda_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Boda bodaId;

    public Presupuesto() {
    }

    public Presupuesto(Integer id) {
        this.id = id;
    }

    public Presupuesto(Integer id, String titulo, int valorEstimado, int valorReal) {
        this.id = id;
        this.titulo = titulo;
        this.valorEstimado = valorEstimado;
        this.valorReal = valorReal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getValorEstimado() {
        return valorEstimado;
    }

    public void setValorEstimado(int valorEstimado) {
        this.valorEstimado = valorEstimado;
    }

    public int getValorReal() {
        return valorReal;
    }

    public void setValorReal(int valorReal) {
        this.valorReal = valorReal;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boda getBodaId() {
        return bodaId;
    }

    public void setBodaId(Boda bodaId) {
        this.bodaId = bodaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Presupuesto)) {
            return false;
        }
        Presupuesto other = (Presupuesto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.marryapp.entity.Presupuesto[ id=" + id + " ]";
    }
    
}
