/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marryapp.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author m4605
 */
@Entity
@Table(name = "categoria_regalo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CategoriaRegalo.findAll", query = "SELECT c FROM CategoriaRegalo c"),
    @NamedQuery(name = "CategoriaRegalo.findById", query = "SELECT c FROM CategoriaRegalo c WHERE c.id = :id"),
    @NamedQuery(name = "CategoriaRegalo.findByNombre", query = "SELECT c FROM CategoriaRegalo c WHERE c.nombre = :nombre")})
public class CategoriaRegalo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre")
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categoriaRegaloId")
    private Collection<Regalo> regaloCollection;

    public CategoriaRegalo() {
    }

    public CategoriaRegalo(Integer id) {
        this.id = id;
    }

    public CategoriaRegalo(Integer id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public Collection<Regalo> getRegaloCollection() {
        return regaloCollection;
    }

    public void setRegaloCollection(Collection<Regalo> regaloCollection) {
        this.regaloCollection = regaloCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoriaRegalo)) {
            return false;
        }
        CategoriaRegalo other = (CategoriaRegalo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.marryapp.entity.CategoriaRegalo[ id=" + id + " ]";
    }
    
}
