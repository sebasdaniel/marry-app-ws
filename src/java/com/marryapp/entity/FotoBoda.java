/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marryapp.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author m4605
 */
@Entity
@Table(name = "foto_boda")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FotoBoda.findAll", query = "SELECT f FROM FotoBoda f"),
    @NamedQuery(name = "FotoBoda.findById", query = "SELECT f FROM FotoBoda f WHERE f.id = :id"),
    @NamedQuery(name = "FotoBoda.findByNombre", query = "SELECT f FROM FotoBoda f WHERE f.nombre = :nombre"),
    @NamedQuery(name = "FotoBoda.findByDescripcion", query = "SELECT f FROM FotoBoda f WHERE f.descripcion = :descripcion")})
public class FotoBoda implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 200)
    @Column(name = "descripcion")
    private String descripcion;
    @JoinColumn(name = "participante_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Participante participanteId;
    @JoinColumn(name = "boda_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Boda bodaId;

    public FotoBoda() {
    }

    public FotoBoda(Integer id) {
        this.id = id;
    }

    public FotoBoda(Integer id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Participante getParticipanteId() {
        return participanteId;
    }

    public void setParticipanteId(Participante participanteId) {
        this.participanteId = participanteId;
    }

    public Boda getBodaId() {
        return bodaId;
    }

    public void setBodaId(Boda bodaId) {
        this.bodaId = bodaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FotoBoda)) {
            return false;
        }
        FotoBoda other = (FotoBoda) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.marryapp.entity.FotoBoda[ id=" + id + " ]";
    }
    
}
