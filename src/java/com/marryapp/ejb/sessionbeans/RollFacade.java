/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marryapp.ejb.sessionbeans;

import com.marryapp.entity.Roll;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author m4605
 */
@Stateless
public class RollFacade extends AbstractFacade<Roll> {
    @PersistenceContext(unitName = "MarryAppPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RollFacade() {
        super(Roll.class);
    }
    
}
