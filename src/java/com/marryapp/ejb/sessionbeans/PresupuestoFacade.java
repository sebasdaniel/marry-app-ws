/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marryapp.ejb.sessionbeans;

import com.marryapp.entity.Presupuesto;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author m4605
 */
@Stateless
public class PresupuestoFacade extends AbstractFacade<Presupuesto> {
    @PersistenceContext(unitName = "MarryAppPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PresupuestoFacade() {
        super(Presupuesto.class);
    }
    
}
