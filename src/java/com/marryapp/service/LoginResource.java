/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marryapp.service;

import com.marryapp.ejb.sessionbeans.UsuarioFacade;
import com.marryapp.entity.Usuario;
import javax.ejb.EJB;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author m4605
 */
@Path("/login")
public class LoginResource {

    @Context
    private UriInfo context;
    
    @EJB
    private UsuarioFacade usuarioFacade;

    /**
     * Creates a new instance of LoginResource
     */
    public LoginResource() {
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Usuario posLogin(@FormParam("correo") String correo,
            @FormParam("clave") String clave) {
        //TODO: manejo de errores
        return usuarioFacade.login(correo, clave);
    }
    
    @POST
    @Path("/recuperar")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_PLAIN)
    public String posRecuperar(@FormParam("correo") String correo) {
        return "No implementado";
    }
}
