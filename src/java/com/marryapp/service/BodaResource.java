/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marryapp.service;

import com.marryapp.ejb.sessionbeans.BodaFacade;
import com.marryapp.ejb.sessionbeans.ParticipanteFacade;
import com.marryapp.entity.Boda;
import com.marryapp.entity.Participante;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author m4605
 */
@Path("/bodas")
public class BodaResource {

    @Context
    private UriInfo context;
    
    @EJB
    private BodaFacade bodaFacade;
    
    @EJB
    private ParticipanteFacade participanteFacade;

    /**
     * Creates a new instance of BodaResource
     */
    public BodaResource() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Boda> getBodas() {
        
        List<Boda> bodas = bodaFacade.findAll();
        return bodas;
    }
    
    @GET
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Boda getBoda(@FormParam("id") int id) {
        
        Boda boda = bodaFacade.find(id);
        return boda;
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearBoda(
            @FormParam("fecha") String fecha,
            @FormParam("ciudad") String ciudad,
            @FormParam("pais") String pais) {
        
        Boda boda = new Boda();
        
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = dateFormat.parse(fecha);
            
            boda.setFecha(date);
            boda.setCiudad(ciudad);
            boda.setPais(pais);
            
            bodaFacade.create(boda);
            
        } catch (Exception ex) {
            Logger.getLogger(BodaResource.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(HttpServletResponse.SC_BAD_REQUEST).build();
        }
        
        return Response.ok("{id: " + boda.getId() + "}").build();
    }
    
    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarBoda(
            @PathParam("id") int id,
            @FormParam("fecha") String fecha,
            @FormParam("ciudad") String ciudad,
            @FormParam("pais") String pais) {
        
        Boda boda = new Boda();
        
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = dateFormat.parse(fecha);
            
            boda.setFecha(date);
            boda.setCiudad(ciudad);
            boda.setPais(pais);
            
            bodaFacade.create(boda);
            
        } catch (Exception ex) {
            Logger.getLogger(BodaResource.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(HttpServletResponse.SC_BAD_REQUEST).build();
        }
        
        return Response.ok("{id: " + boda.getId() + "}").build();
    }
}
