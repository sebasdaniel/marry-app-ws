/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marryapp.service;

import com.marryapp.ejb.sessionbeans.UsuarioFacade;
import com.marryapp.entity.Usuario;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author m4605
 */
@Path("/usuarios")
public class UsuarioResource {

    @Context
    private UriInfo context;
    
    @EJB
    private UsuarioFacade usuarioFacade;

    /**
     * Creates a new instance of UsuarioResource
     */
    public UsuarioResource() {
    }

    /**
     * Lista de Usuarios
     * @return an instance of Usuario
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Usuario> getUsuarios() {
        
        List<Usuario> usuarios = usuarioFacade.findAll();
        return usuarios;
    }
    
    /**
     * Usuario identificado por el id pasado como parametro
     * @param id de usuario
     * @return instancia de Usuario
     */
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Usuario getUsuario(@PathParam("id") int id){
        
        Usuario usuario = usuarioFacade.find(id);
        return usuario;
    }
    
    // Crea un nuevo usuario
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED) // if need upload image use form-data
    @Produces(MediaType.APPLICATION_JSON)
    public String crearUsuario(
            @FormParam("correo") String correo,
            @FormParam("clave") String clave,
            @FormParam("nombres") String nombres,
            @FormParam("apellidos") String apellidos,
            @FormParam("sexo") String sexo,
            @FormParam("telefono") String telefono,
            @FormParam("ciudad") String ciudad,
            @Context HttpServletResponse response) throws IOException {
        
        Usuario usuario = new Usuario();
        
        usuario.setCorreo(correo);
        usuario.setClave(clave);
        usuario.setNombres(nombres);
        usuario.setApellidos(apellidos);
        usuario.setSexo(sexo);
        usuario.setTelefono(telefono);
        usuario.setCiudad(ciudad);
        
        try{
            usuarioFacade.create(usuario);
        } catch (Exception e) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            System.err.println("Error al crear usuario: " + e);
        }
        
        response.setStatus(HttpServletResponse.SC_CREATED);
        return "{id: " + usuario.getId() + "}";
    }
    
    // Edita un usuario
    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED) // if need upload image use form-data
    @Produces(MediaType.APPLICATION_JSON)
    public String editarUsuario(
            @PathParam("id") int id,
            @FormParam("correo") String correo,
            @FormParam("clave") String clave,
            @FormParam("nombres") String nombres,
            @FormParam("apellidos") String apellidos,
            @FormParam("sexo") String sexo,
            @FormParam("telefono") String telefono,
            @FormParam("ciudad") String ciudad,
            @Context HttpServletResponse response) throws IOException {
        
        try{
            Usuario usuario = usuarioFacade.find(id);

            usuario.setCorreo(correo);
            usuario.setClave(clave);
            usuario.setNombres(nombres);
            usuario.setApellidos(apellidos);
            usuario.setSexo(sexo);
            usuario.setTelefono(telefono);
            usuario.setCiudad(ciudad);
            
            usuarioFacade.edit(usuario);
            
        } catch (Exception e) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            System.err.println("Error al crear usuario: " + e);
        }
        //response.setStatus(HttpServletResponse.SC_OK);
        return "{resultado: 'Editado correctamente'}";
    }

}
